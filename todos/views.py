from django.shortcuts import get_object_or_404, render, redirect
from todos.forms import TodoListForm
from todos.models import TodoList, TodoItem


# Create your views here.
def todolist_view(request):
    todolist = TodoList.objects.all()
    context = {
        # this gets referenced as "todolist.all" in list.html
        "todolist": todolist,
    }
    # name of this URL is todo_list_list
    return render(request, "todos/list.html", context)


def todolist_detailview(request, id):
    todoitem = get_object_or_404(TodoList, id=id)
    context = {
        "todoitem": todoitem,
    }
    # name of this URL is todo_list_detail
    return render(request, "todos/detail.html", context)


def todolist_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
    if form.is_valid():
        # To redirect to the detail view of the model, use this:
        form.save()
        return redirect("todos/detail.html")

        # To add something to the model, like setting a user,
        # use something like this:
        #
        # model_instance = form.save(commit=False)
        # model_instance.user = request.user
        # model_instance.save()
        # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm()
        context = {"form": form}

    return render(request, "todos/create.html", context)
