from django.urls import path
from todos.views import todolist_create, todolist_detailview, todolist_view

urlpatterns = [
    path("", todolist_view, name="todo_list_list"),
    path("<int:id>/", todolist_detailview, name="todo_list_detail"),
    path("create/", todolist_create, name="create_todolist"),
]
